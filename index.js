const express = require("express");
const logger = require("morgan");
const concat = require("./concat");
const md5 = require("md5");
const authHeader = require("./middleware/auth");

const app = express();
const port = 8002;

app.use(express.json());
app.use(logger("combined"));

app.get("/", (req, res) => {
  res.set("Content-Type", "text/html");
  res.send(Buffer.from("<h1>My First Express App - Author: Matej Ruzic</h1>"));
});

app.get("/not-found", (req, res) => {
  res.status(404);
  res.set("Content-Type", "text/html");
  res.send(Buffer.from("<h3>Sorry. Page is not found</h3>"));
});

app.get("/error", (req, res, next) => {
  try {
    console.log("ERROR");
    throw new Error("BROKEN");
  } catch (err) {
    next(err);
  }
});

app.get("/student-data", (req, res) => {
  const { firstName, lastName, email } = req.query;

  let data = concat.concat(firstName, lastName, email);

  res.json({ data });
});

app.post("/authorization", async (req, res, next) => {
  try {
    const { firstName, lastName, id, email } = req.body;

    let data = id.concat(`_${firstName}_${lastName}_${email}`);

    const hashed = md5(data);

    res.setHeader("Authorization", hashed);

    res.json(data);
  } catch (err) {
    next(err);
  }
});

app.get(
  "/private-route",
  authHeader.authHeader,
  authHeader.authUser,
  (req, res) => {
    req.customAuthHeader
      ? res.json({ authHeaderExist: true, value: req.customAuthHeader })
      : res.json({ authHeaderExist: false, value: "" });
  }
);

app.use(function (err, req, res, next) {
  console.error(err.message);
  res.status(500).send("Error!");
});

app.listen(port, () => {
  console.log(`Listening on port ${port}!`);
});
